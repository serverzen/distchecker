.. -*-rst-*-

=========
 Changes
=========

1.0 - Dec 18, 2012
==================

  * Cleaned up a little

  * Posted to bitbucket

0.2 - Mar 3, 2011
=================

  * The ``test_sdist`` command no longer needs a directory argument

  * Directory structure reworked -- tests added

0.1.1 - Feb 26, 2011
====================

  * Made compare_sdist a little easier to use

0.1 - Dec 2, 2010
=================

  * First release
